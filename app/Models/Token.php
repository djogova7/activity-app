<?php

// app/Models/Token.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = 'tokens'; // Specify the table name

    protected $fillable = [
        'token',
        'user_id',
        'expires_at',
    ];

    protected $dates = ['expires_at', 'created_at'];

    // Define the relationships
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
