<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ReportMail extends Mailable
{
    use Queueable, SerializesModels; // ok ti treba da stavis novo kopce vo /activities
    // e sega. Koga ke se stisne kopce, ke se praka na back-end date range neli koj so selektiral user
    // i ke go imame i user_id -to koe e na segasniot user neli. Ova go sfakas neli ?da
    // e sega za samiot link, moze da napravime nova tabela na primer mail_tokens
    // kade ke imas token column, used_at
    // koga ke go prakas mail-ot ke generiras nov token i ke go stavis vo nov zapis neli vo tabelata
    // isto link-ot ke bide nova ruta na primer /mail-report kade so ke imas {token} param
    // koj ke ti pokazuva kon daden token vo tabelata neli. Ako e first time use togas ke dozvolis pristap
    // i ke vides spored selected date range (i ova ke treba da go zacuvas nekako vo mail_tokens zaedno so user_id)
    // ke vratis obicen view spored tie activies neli da gi izlistas
    // ova ke ti bide nova ruta so ne treba auth, kako api ruta neli sfakas ?nz ke poscnem od pocetok i ke vidime ok
    // mislam deka e ova ok resenie tuka oksiii toa e toa :D okic ajde ke si pocnem ja tyajdee good luck ty :D

    /**
     * Create a new message instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Report Mail',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'emails.reports',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
