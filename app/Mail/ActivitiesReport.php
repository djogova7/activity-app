<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ActivitiesReport extends Mailable
{
    use Queueable, SerializesModels;
    public $url;
    public $activities;
    public $fromDate;
    public $toDate;

    /**
     * Create a new message instance.
     */
    public function __construct($url,  $fromDate, $toDate)
    {
        $this->url = $url;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
    }

    /**
     * @throws \ReflectionException
     */
    public function build()
    {
            return $this->subject('Activities Report')->view('emails.activities-report')
                ->with([
                    'url' => $this->url,
                    'fromDate' => $this->fromDate,
                    'toDate' => $this->toDate,
                ])
                ->withSwiftMessage(function ($message) {
                    $message->getBody()->setContent('Your email content here, including URL: ' . $this->url);
                });
    }
    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Activities Report',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
