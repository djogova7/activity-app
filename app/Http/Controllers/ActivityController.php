<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreActivityRequest;
use App\Mail\ActivitiesReport;
use App\Models\Activity;
use App\Models\Token;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ActivityController extends Controller
{

    public function index(Request $request)
    {
        $user = auth()->user();
        $fromDate = $request->input('fromDate') ?? '';
        $toDate = $request->input('toDate') ?? '';

        $activities = Activity::where('user_id', $user->id)
            ->when($fromDate, function ($query) use ($fromDate) {
                return $query->where('activity_date', '>=', $fromDate);
            })
            ->when($toDate, function ($query) use ($toDate) {
                return $query->where('activity_date', '<=', $toDate);
            })
            ->orderBy('activity_date', 'desc')
            ->get();

        return view('activities.index', compact('activities'));
    }

    public function create()
    {
        return view('activities.create');
    }

    public function store (StoreActivityRequest $request , User $user): RedirectResponse
    {
        $validatedData = $request->all();
        $activity = new Activity([
            'activity_date' => $validatedData['activity_date'],
            'time_spent' => $validatedData['time_spent'],
            'description' => $validatedData['description'],
            'user_id' => auth()->user()->id
        ]);
        $activity->save();
        return redirect()->route('activities.index')
            ->with('success', 'Activity added successfully');

    }

    public function edit(Activity $activity)
    {
        return view('activities.edit', compact('activity'));
    }

    public function update(Request $request, Activity $activity): RedirectResponse
    {
        $request->validate([
            'activity_date' => 'required|date',
            'description' => 'required|string|max:255',
        ]);

        $activity->update([
            'activity_date' => $request->input('activity_date'),
            'description' => $request->input('description'),
        ]);

        return redirect()->route('activities.index')->with('success', 'Activity updated successfully');
    }

    public function destroy(Activity $activity): RedirectResponse
    {
        $activity->delete();

        return redirect()->route('activities.index')->with('success', 'Activity deleted successfully');
    }



    public function sendEmail(Request $request)
    {
        $user = auth()->user();
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
        $activityReport = $request->input('activityReport');

        $reportToken = md5(uniqid('', true));

        Token::create([
            'token' => $reportToken,
            'user_id' => $user->id,
            'expires_at' => now()->addHours(2),
        ]);

         $url = route('activities.report', [
            'token' => $reportToken,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'activityReport' => $activityReport,
        ]);
        Mail::to($user->email)->send(new ActivitiesReport($url,$fromDate,$toDate));

        return redirect()->back()->with('success', 'The report is sent successfully.');
    }



    public function reportPage(Request $request, $token)
    {
        $dbToken = Token::where('token', $token)->first();

        if (!$dbToken || $dbToken->expires_at <= now()) {
            return abort(403);
        }

        $user = User::find($dbToken->user_id);

        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
        $activityReport= $request->input('activityReport');

        $activities = Activity::where('user_id', $user->id)
            ->when($fromDate, function ($query) use ($fromDate) {
                return $query->where('activity_date', '>=', $fromDate);
            })
            ->when($toDate, function ($query) use ($toDate) {
                return $query->where('activity_date', '<=', $toDate);
            })
            ->get();
        if (!$activityReport) {
            $groupedActivities = $activities->groupBy(function ($activity) {
                return Carbon::parse($activity->activity_date)->format('Y-m-d');
            });
        }
        else {
            $groupedActivities = null;
        }

        return view('activities.report', compact('activities', 'groupedActivities'));
    }

}
