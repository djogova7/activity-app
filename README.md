Activity App

This project contains simple Activity api where we can perform CRUD operation on list of activities, 
and send reports to email of a specific user.

Main technologies used are PHP and Laravel framework. More info about django on the following link:
https://laravel.com/

Email report testing: I used mailtrap, you can create an account and connect it to your mail here: https://mailtrap.io/

Running the project :
Change into the project directory, and publish the Laravel Sail Docker Compose configuration files using the following command:
php artisan sail:install

The command will prompt the person to specify which services they want to include in their Docker Compose file.Select the default options by pressing Enter.

To start the Laravel Sail development environment, you can run the following command:

./vendor/bin/sail up

You can configure the database connection in the .env file using the settings provided in the .env.example file.

Run the database migrations to set up the database schema: 
./vendor/bin/sail artisan migrate

Migrate the database tables:
./vendor/bin/sail artisan db:seed

Now you can open a web browser and navigate to http://localhost/ to access the Laravel application.You will be redirected to login page.
For testing, you can use the following user:
test@gmail.com 
ChangeMe123
