<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Activity>
 */
class ActivityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'activity_date'=>$this->faker->dateTimeBetween('-1 month', now())->format('Y-m-d'),
            'time_spent' => $this->faker->numberBetween(30, 90),
            'description' =>$this->faker->text(100),
            'user_id'=> User::factory()
        ];
    }
}
