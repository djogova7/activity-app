@extends('layouts.app')

@section('content')
    @include('flash::message')
    <form method="GET" action="{{ route('activities.index') }}" class="filter-form">
        @csrf
        <label for="fromDate">From:</label>
        <input type="date" name="fromDate" id="fromDate" value="{{ request('fromDate') }}" required>

        <label for="toDate">To:</label>
        <input type="date" name="toDate" id="toDate" value="{{ request('toDate') }}" required>


        <button type="submit" class="filter-button">Apply Filter</button>
        <div class="send-email-buttons">
            <a href="{{ route('activities.sendEmail', ['fromDate' => request('fromDate'), 'toDate' => request('toDate'), 'activityReport' => true]) }}" class="outlined-button">Send Activity Report</a>
            <a href="{{ route('activities.sendEmail', ['fromDate' => request('fromDate'), 'toDate' => request('toDate'), 'activityReport' => false]) }}" class="outlined-button">Send Print Report</a>
        </div>
    </form>

    <div>
        <div class="table-container">
            <table class="table">
                <thead>
                <tr>
                    <th colspan="2">Activities</th>
                    <th>
                        <a href="{{ route('activities.create') }}" class="add-button">Add Activity</a>
                    </th>
                </tr>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($activities as $activity)
                    <tr>
                        <td>{{ $activity->activity_date }}</td>
                        <td>{{ $activity->description }}</td>
                        <td>
                            <div class="button-container">
                                <a href="{{ route('activities.edit', ['activity' => $activity->id]) }}" class="edit-button">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <form method="POST" action="{{ route('activities.destroy', ['activity' => $activity->id]) }}" class="delete-form">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="delete-button">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .table-container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table th, .table td {
            border: 1px solid #ddd;
            padding: 10px;
            text-align: left;
        }

        .table th {
            background-color: #f2f2f2;
        }

        .table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .table tr:hover {
            background-color: #e0e0e0;
        }

        .add-button {

            color: green;
            padding: 10px 20px;
            border: none;
            cursor: pointer;
            text-decoration: none;
            font-weight: bold;
            text-decoration: underline;
        }

        .add-button:hover {
            font-size: 18px;
            background-color: transparent;
        }

         .button-container {
             display: flex;
             gap: 10px;
         }


        .edit-button {
            background-color: transparent;
            color: #007bff;
            border: none;
            cursor: pointer;
        }

        .edit-button:hover {
            color: #0056b3;
        }


        .delete-button {
            background-color: transparent;
            color: #dc3545;
            border: none;
            cursor: pointer;
        }

        .delete-button:hover {
            color: #c82333;
        }

        .filter-form {
            display: flex;
            align-items: center;
            gap: 10px;
            margin-bottom: 5px;
            padding: 5px;
            background-color: #f2f2f2;
            border: 1px solid #ddd;
            border-radius: 5px;
        }

        .filter-form label {
            font-weight: bold;
            font-size: 12px;

        }

        .filter-form input[type="date"] {
            font-size: 12px;
            padding: 1px;
        }

        .filter-button {
            background-color: transparent;
            padding: 5px 10px;
            border-radius: 5px;
            color: green;
            font-weight: bold;
            text-decoration: none;
            font-size: 12px;
            transition: background-color 0.3s, color 0.3s, transform 0.3s;
        }
        .filter-button:hover {
            font-size: 16px;
            background-color: transparent;
        }
        /* Outlined button styles */
        .outlined-button {
            display: inline-block;
            padding: 5px 10px; /* Reduce the padding for a smaller button */
            text-align: center;
            text-decoration: none;
            background-color: transparent;
            border: 2px solid transparent;
            border-left: 2px solid #000;
            border-right: 2px solid #000;
            color: #000;
            border-radius: 5px;
            font-size: 12px;
            transition: background-color 0.3s, color 0.3s;
        }

        .outlined-button:hover {
            background-color: #000;
            color: #fff;
            transform: scale(1.1);
        }
        .outlined-button:hover {
            background-color: #007bff;
            color: #fff;
        }
        #success-message {
            display: none;
            background-color: green;
            color: white;
            padding: 10px;
            text-align: center;
            position: fixed;
            top: 0;
            left: 50%;
            transform: translateX(-50%);
            z-index: 9999;
            border-radius: 5px;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('#filter-form').on('submit', function(event) {
                const fromDate = new Date($('#fromDate').val());
                const toDate = new Date($('#toDate').val());

                if (toDate < fromDate) {
                    event.preventDefault(); // Prevent form submission
                    alert('The "To" date cannot be earlier than the "From" date.');
                }
            });
        });
    </script>







@endsection
