@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6"> <!-- Set the width to half of the page -->
                <div class="card">
                    <div class="card-header">Add New Activity</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('activities.store') }}">
                            @csrf

                            <div class="form-group">
                                <label for="activity_date">Activity Date</label>
                                <input type="date" class="form-control" id="activity_date" name="activity_date" required>
                            </div>

                            <div class="form-group">
                                <label for="time_spent">Time Spent (in minutes)</label>
                                <input type="number" class="form-control" id="time_spent" name="time_spent" required>
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description" required></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .form-control {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        .btn {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 10px;
            margin: 10px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        .btn:hover {
            background-color: #45a049;
        }

        .card {
            margin: 200px;
            margin-top: 20px;
            padding: 20px;
            border: 1px solid #e0e0e0;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

    </style>
@endsection
