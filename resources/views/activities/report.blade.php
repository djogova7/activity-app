@extends('layouts.app')

@section('content')
    @if ($groupedActivities)
        <div class="table-container">
            <table class="table">
                <thead>
                <tr>
                    <th>Day</th>
                    <th>Activities</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($groupedActivities as $date => $activities)
                    <tr>
                        <td>{{ $date }}</td>
                        <td>
                            <ul>
                                @foreach ($activities as $activity)
                                    <li>
                                        <span style="margin-left: 10px;">&bull;</span> {{ $activity->description }}
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else

    <div>
        <div class="table-container">
            <table class="table">
                <thead>
                <tr>
                    <th colspan="2">Activities</th>
                </tr>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($activities as $activity)
                    <tr>
                        <td>{{ $activity->activity_date }}</td>
                        <td>{{ $activity->description }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
    <style>
        .table-container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table th, .table td {
            border: 1px solid #ddd;
            padding: 10px;
            text-align: left;
        }

        .table th {
            background-color: #f2f2f2;
        }

        .table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .table tr:hover {
            background-color: #e0e0e0;
        }

        .add-button {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border: none;
            cursor: pointer;
            text-decoration: none;
        }

        .add-button:hover {
            background-color: #45a049;
        }
        /* Edit and Delete button container */
        .button-container {
            display: flex;
            gap: 10px; /* Adjust the gap between the buttons as needed */
        }

        /* Edit button style (you can adjust this if needed) */
        .edit-button {
            background-color: transparent;
            color: #007bff; /* Blue color for the icon */
            border: none;
            cursor: pointer;
        }

        .edit-button:hover {
            color: #0056b3; /* Darker blue on hover */
        }

        /* Delete button style */
        .delete-button {
            background-color: transparent;
            color: #dc3545; /* Red color for the icon */
            border: none;
            cursor: pointer;
        }

        .delete-button:hover {
            color: #c82333; /* Darker red on hover */
        }
        /* Smaller filter form style with reduced padding and font size */
        .filter-form {
            display: flex;
            align-items: center;
            gap: 10px;
            margin-bottom: 5px;
            padding: 5px; /* Reduce the padding to make it smaller */
            background-color: #f2f2f2;
            border: 1px solid #ddd;
            border-radius: 5px;
        }

        .filter-form label {
            font-weight: bold;
            font-size: 12px; /* Reduce the font size */

        }

        .filter-form input[type="date"] {
            font-size: 12px; /* Reduce the font size for date inputs */
            padding: 1px;
        }

        .filter-button {
            background-color: #007bff;
            color: white;
            padding: 5px 12px; /* Reduce the button padding */
            border: none;
            cursor: pointer;
            font-size: 12px; /* Reduce the font size for the button text */
        }
        .filter-button:hover {
            background-color: #0056b3;
        }
        .export-button {
            float: right; /* Move the button to the right */
        }

    </style>

@endsection
